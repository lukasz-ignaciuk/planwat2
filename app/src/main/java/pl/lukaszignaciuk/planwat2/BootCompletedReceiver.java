package pl.lukaszignaciuk.planwat2;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import java.util.Calendar;

public class BootCompletedReceiver extends BroadcastReceiver {

    private final String TAG = "BootCompletedTAG";

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals("android.intent.action.BOOT_COMPLETED")){
            Intent i = new Intent("doUpdate");

            context.startService(new Intent(context, TimetableChangeCheck.class));

            int delay = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(context).getString("period", "24"));
            long period = 1000L * 60 * 60 * delay;

            AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, i, PendingIntent.FLAG_CANCEL_CURRENT);
            alarm.setRepeating(AlarmManager.RTC_WAKEUP, Calendar.getInstance().getTimeInMillis() + 1000L * 60 * 2, period, pendingIntent);
        }
    }
}
