package pl.lukaszignaciuk.planwat2;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import org.apache.http.util.EncodingUtils;

public class WebViewActivity extends Activity {

    private WebView webView;

    private boolean planLoaded;
    private boolean split = false;
    private String sessionID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web_view);

        webView = (WebView) findViewById(R.id.webView1);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.setWebViewClient(new MyWebViewClient());

        startAndLogin();
    }

    private void startAndLogin(){
        String login = PreferenceManager.getDefaultSharedPreferences(this).getString("lgn", "");
        String password = PreferenceManager.getDefaultSharedPreferences(this).getString("pwd", "");
        String url = "https://s1.wcy.wat.edu.pl/ed/index.php?sid=ea644ce6e727ef5320c56c2034bece7f";
        String postData = "formname=login&userid="+login+"&password="+password;

        planLoaded = false;
        webView.postUrl(url, EncodingUtils.getBytes(postData, "UTF8"));
    }

    private void loadPlan(){
        String group = PreferenceManager.getDefaultSharedPreferences(this).getString("anyGroup", "");
        String nUrl = "https://s1.wcy.wat.edu.pl/ed/logged.php?" + sessionID + "&mid=328&iid=20145&prn=10&exv=" + group.toUpperCase();

        webView.loadUrl(nUrl);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    private class MyWebViewClient extends WebViewClient {

        final ProgressDialog pd = ProgressDialog.show(WebViewActivity.this, "", "Ładowanie...", true);

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url){
            return false;
        }

        @Override
        public void onReceivedSslError (WebView view, SslErrorHandler handler, SslError error) {
            handler.proceed() ;
        }

        @Override
        public void onPageFinished(WebView view, String url){

            if(url.contains("ea644ce6e727ef5320c56c2034bece7f")){
                webView.loadData("Błędne dane logowania lub brak połączenia z internetem", "text/html; charset=UTF-8", null);
            }
            pd.dismiss();
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon)
        {
            if(url.contains("logged_inc.php") && !split){
                String parts[] = url.split("\\?");
                String sid[] = parts[1].split("&");
                sessionID = sid[0];
                split = true;
                if(!planLoaded){
                    loadPlan();
                }
            }
            pd.show();
        }
    }
}