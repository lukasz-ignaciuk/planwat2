package pl.lukaszignaciuk.planwat2;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

/**
 * Created by Łukasz on 2015-03-25.
 */
public class TimetableCursorAdapter extends CursorAdapter {
    public TimetableCursorAdapter(Context context, Cursor cursor){
        super(context, cursor);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.row_timetable, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView temat, lokalizacja, dataR, dataZ;
        view.setOnClickListener(null);

        temat = (TextView) view.findViewById(R.id.tvTemat);
        lokalizacja = (TextView) view.findViewById(R.id.tvLokalizacja);
        dataR = (TextView) view.findViewById(R.id.tvDataR);
        dataZ = (TextView) view.findViewById(R.id.tvDataZ);

        temat.setText(cursor.getString(TimetableDbAdapter.TEMAT_COLUMN));
        lokalizacja.setText(cursor.getString(TimetableDbAdapter.LOC_COLUMN));
        dataR.setText(cursor.getString(TimetableDbAdapter.DATAR_COLUMN) + " godz. " + cursor.getString(TimetableDbAdapter.CZASR_COLUMN));
        dataZ.setText(cursor.getString(TimetableDbAdapter.DATAZ_COLUMN) + " godz. " + cursor.getString(TimetableDbAdapter.CZASZ_COLUMN));
    }
}
