package pl.lukaszignaciuk.planwat2;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends ActionBarActivity {

    private static final long MILISECONDS_PER_DAY = 1000L*60*60*24;

    private GestureDetectorCompat mDetector;
    private TimetableDbAdapter timetableDbAdapter;
    private Cursor timetableCursor;
    private ListView listView;
    private TextView tvHeader;
    private TimetableCursorAdapter cursorAdapter;
    private Date timetableForDate;
    private boolean inSettings;
    private ProgressDialog pd;
    private BroadcastReceiver mMessageReceiver;
    private boolean classTimetable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.listView);
        tvHeader = (TextView) findViewById(R.id.tvHeader);
        if(savedInstanceState != null && !savedInstanceState.isEmpty()) {
            inSettings = savedInstanceState.getBoolean("inSettings", false);
            classTimetable = savedInstanceState.getBoolean("classTimetable", false);
            timetableForDate = new Date(savedInstanceState.getLong("timetableForDate"));
        } else {
            inSettings = false;
            classTimetable = false;
            timetableForDate = new Date();
        }

        mDetector = new GestureDetectorCompat(getApplicationContext(), new flingListener());

        timetableDbAdapter = new TimetableDbAdapter(getApplicationContext());
        timetableDbAdapter.open();

        mMessageReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.getAction().equals("fillList")) {
                    fillListView();
                    if(pd != null && pd.isShowing()) pd.dismiss();
                }
                if(intent.getAction().equals("loginError")){
                    if(pd != null && pd.isShowing()) pd.dismiss();
                    Toast tst = Toast.makeText(getApplicationContext(), getString(R.string.login_error), Toast.LENGTH_SHORT);
                    tst.show();
                }
                if(intent.getAction().equals("downloadError")){
                    if(pd != null && pd.isShowing()) pd.dismiss();
                    Toast tst = Toast.makeText(getApplicationContext(), getString(R.string.download_error), Toast.LENGTH_SHORT);
                    tst.show();
                }
            }
        };

        IntentFilter intentFiler = new IntentFilter();
        intentFiler.addAction("fillList");
        intentFiler.addAction("loginError");
        intentFiler.addAction("downloadError");

        registerReceiver(mMessageReceiver, intentFiler);

        startService(new Intent(this, TimetableChangeCheck.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
        timetableForDate = Calendar.getInstance().getTime();
        fillListView();
    }

    protected void fillListView() {
        classTimetable = false;

        timetableCursor = timetableDbAdapter.getSpecifiedDayTimetable(new SimpleDateFormat("yyyy-MM-dd").format(timetableForDate), PreferenceManager.getDefaultSharedPreferences(this).getBoolean("displayLectures", true));
        timetableCursor.moveToFirst();
        cursorAdapter = new TimetableCursorAdapter(getApplicationContext(), timetableCursor);

        if(timetableCursor.isAfterLast()){
            tvHeader.setText("Dnia " + new SimpleDateFormat("yyyy-MM-dd").format(timetableForDate) + " brak zajęć!");
            listView.setAdapter(cursorAdapter);
        }
        else{
            tvHeader.setText("Plan zajęć w dniu " + new SimpleDateFormat("yyyy-MM-dd").format(timetableForDate));
            listView.setAdapter(cursorAdapter);
        }
    }

    protected void fillListView(String className) {
        classTimetable = true;

        timetableCursor = timetableDbAdapter.getClassTimetable(className);
        timetableCursor.moveToFirst();
        cursorAdapter = new TimetableCursorAdapter(getApplicationContext(), timetableCursor);
        tvHeader.setText(className);

        listView.setAdapter(cursorAdapter);
    }

    @Override
    protected void onDestroy() {
        if(timetableDbAdapter != null)
            timetableDbAdapter.close();
        super.onDestroy();
        unregisterReceiver(mMessageReceiver);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("inSettings", inSettings);
        outState.putBoolean("classTimetable", classTimetable);
        outState.putLong("timetableForDate", timetableForDate.getTime());
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        super.dispatchTouchEvent(event);
        return mDetector.onTouchEvent(event);
    }

    @Override
    public void onBackPressed() {
        if(inSettings){
            inSettings = false;
            getFragmentManager().popBackStack();
        } else if(classTimetable){
            classTimetable = false;
            fillListView();
        }
        else super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch(id){
            case R.id.action_settings: {
                inSettings = true;
                getFragmentManager()
                        .beginTransaction()
                        .add(android.R.id.content, new PreferencesFragment())
                        .addToBackStack("settings")
                        .commit();
                break;
            }
            case R.id.action_anyGroupTimetable: {
                final EditText txtUrl = new EditText(this);
                txtUrl.setText(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("anyGroup", "i2o1s1"));

                new AlertDialog.Builder(this)
                        .setTitle(R.string.anyGroupTitle)
                        .setView(txtUrl)
                        .setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                String url = txtUrl.getText().toString();
                                PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("anyGroup", url).commit();
                                Intent i = new Intent(MainActivity.this, WebViewActivity.class);
                                startActivity(i);
                            }
                        })
                        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                            }
                        })
                        .show();
                break;
            }
            case R.id.action_download: {
                pd = ProgressDialog.show(MainActivity.this, "", MainActivity.this.getString(R.string.loading_string), true);
                Intent intent = new Intent();
                intent.setAction("doUpdate");
                sendBroadcast(intent);
                break;
            }
            case R.id.action_date: {
                Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dpd = new DatePickerDialog(this,
                        new DatePickerDialog.OnDateSetListener(){
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                Calendar newCal = Calendar.getInstance();
                                newCal.set(year, monthOfYear, dayOfMonth);
                                timetableForDate.setTime(newCal.getTimeInMillis());
                                fillListView();
                            }
                        },
                        year,
                        month,
                        day);

                dpd.show();
                break;
            }
            case R.id.action_timetableAutoUpdate: {
                final NumberPicker np = new NumberPicker(this);
                final int currentVal = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("period", "24"));
                np.setMaxValue(24);
                np.setMinValue(1);
                np.setValue(currentVal);

                new AlertDialog.Builder(this)
                        .setTitle(R.string.delayPickerTitle)
                        .setView(np)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                String delay = "";
                                delay += np.getValue();
                                PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("period", delay).commit();
                                startAutoupdate(currentVal);
                            }
                        })
                        .show();

                break;
            }
            case R.id.action_info: {
                new AlertDialog.Builder(this)
                        .setTitle(R.string.info)
                        .setMessage(getString(R.string.info_message)+"\n"+getString(R.string.update_message)+"\n"+PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("lastUpdate", "brak"))
                        .setPositiveButton("OK", null)
                        .show();
                break;
            }
            case R.id.action_specifiedClassTimetable: {
                final CharSequence[] classNames = timetableDbAdapter.getClassNames();

                new AlertDialog.Builder(this)
                        .setTitle(R.string.specifiedClassTimetableTitle)
                        .setItems(classNames, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichChoosen) {
                                fillListView((String)classNames[whichChoosen]);
                            }
                        })
                        .show();
                break;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private void startAutoupdate(int lastValue) {
        Intent intent = new Intent();
        intent.setAction("doUpdate");

        int delay = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("period", "24"));
        long period = 1000L * 60 * 60 * delay;

        AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        if(PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_NO_CREATE) == null || delay != lastValue){  //autoaktualizacja nie byla zaplanowana lub zmiana czasu
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
            alarm.setRepeating(AlarmManager.RTC_WAKEUP, Calendar.getInstance().getTimeInMillis(), period, pendingIntent);
            Toast tst = Toast.makeText(getApplicationContext(), getString(R.string.update_set, delay), Toast.LENGTH_SHORT);
            tst.show();
            Log.i("AUTOUPDATE", "alarm set to " + delay + " hours");
        }
    }

    class flingListener extends GestureDetector.SimpleOnGestureListener{
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            if(e1.getX() - e2.getX() > 120 && Math.abs(velocityX) > 200) {
                timetableForDate.setTime(timetableForDate.getTime() + MILISECONDS_PER_DAY);
                fillListView();
            }  else if (e2.getX() - e1.getX() > 120 && Math.abs(velocityX) > 200) {
                timetableForDate.setTime(timetableForDate.getTime() - MILISECONDS_PER_DAY);
                fillListView();
            }
            return true;
        }
    }

}

