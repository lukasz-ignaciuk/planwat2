package pl.lukaszignaciuk.planwat2;

import android.content.Context;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Created by Łukasz on 2015-03-28.
 */
public abstract class EDziekanatConnector extends AsyncTask<Void, Void, ArrayList<String>> {

    private String login;
    private String password;
    private String sessionID;
    private String group;
    private Context callingActivityContext;
    private static OkHttpClient client;

    public EDziekanatConnector(Context callingActivityContext) {
        this.callingActivityContext = callingActivityContext;
        group = PreferenceManager.getDefaultSharedPreferences(callingActivityContext).getString("group", "");
        login = PreferenceManager.getDefaultSharedPreferences(callingActivityContext).getString("lgn", "");
        password = PreferenceManager.getDefaultSharedPreferences(callingActivityContext).getString("pwd", "");
        client = new OkHttpClient();
    }

    public abstract void onResponseReceived(ArrayList<String> result);

    @Override
    protected ArrayList<String> doInBackground(Void... params) {
        try {
            Log.d("eDziekanatConnector", "Próbuję się zalogować...");
            trustAllHosts();
            if(login()) {
                return getTimetable();
            } else {
                return null;
            }
        } catch (Exception e) {
            Log.d("eDziekanatConnector", "Wyjebałem się na wyjątku.");
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(ArrayList<String> strings) {
        onResponseReceived(strings);
        super.onPostExecute(strings);
    }

    private boolean login() throws Exception {
        String url = "https://s1.wcy.wat.edu.pl/ed/index.php?sid=ea644ce6e727ef5320c56c2034bece7f";

        RequestBody formBody = new FormEncodingBuilder()
                .add("formname", "login")
                .add("userid", login)
                .add("password", password)
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();

        Response response = client.newCall(request).execute();

        if(response.request().urlString().contains("ea644ce6e727ef5320c56c2034bece7f")){
            return false;
        }

        String parts[] = response.request().urlString().split("\\?");
        String sid[] = parts[1].split("&");
        sessionID = sid[0];

        return true;
    }

    private ArrayList<String> getTimetable() throws Exception {
        String inputLine;
        ArrayList<String> csv = new ArrayList<>();
        BufferedReader get;
        Response response;
        Request request;

        String firstUrl = "https://s1.wcy.wat.edu.pl/ed/logged.php?"+sessionID+"&mid=328&iid=20145&vrf=32820145&rdo=1&pos=0&exv="+group+"&vrf=!120&rdo=1&pos=0";
        String getUrl = "https://s1.wcy.wat.edu.pl/ed/logged.php?"+sessionID+"&mid=328&iid=20145&vrf=32820145&rdo=1&pos=0&exv="+group+"&opr=DTXT";

        request = new Request.Builder()
                .url(firstUrl)
                .build();
        client.newCall(request).execute();

        request = new Request.Builder()
                .url(getUrl)
                .build();

        Thread.sleep(5000);
        response = client.newCall(request).execute();

        get = new BufferedReader(new InputStreamReader(response.body().byteStream(), Charset.forName("windows-1250")));

        if( ((inputLine = get.readLine()) != null) && (inputLine.startsWith("Temat")) ){
            do {
                csv.add(inputLine);
            } while((inputLine = get.readLine()) != null);
        }

        PreferenceManager.getDefaultSharedPreferences(callingActivityContext).edit().putString("lastUpdate", Calendar.getInstance(new Locale("pl-PL")).getTime().toString()).commit();
        return csv;
    }

    //Skopiowana ze stackoverflow metoda tworząca SSLSocketFactory akceptujący wszystkie certyfikaty
    private static void trustAllHosts() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return new java.security.cert.X509Certificate[] {};
            }

            public void checkClientTrusted(X509Certificate[] chain,
                                           String authType) throws CertificateException {
            }

            public void checkServerTrusted(X509Certificate[] chain,
                                           String authType) throws CertificateException {
            }
        } };

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            client.setSslSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
