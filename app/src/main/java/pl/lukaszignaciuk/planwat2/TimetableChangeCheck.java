package pl.lukaszignaciuk.planwat2;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.util.ArrayList;
import java.util.Iterator;

public class TimetableChangeCheck extends Service {
    private static final String TAG_SERVICE = "SERVICE_DEBUG";

    private boolean updateNeeded = false;
    private TimetableDbAdapter timetableDbAdapter;
    private NotificationCompat.Builder mNotification;
    private NotificationManager mNotificationManager;

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        timetableDbAdapter = new TimetableDbAdapter(getApplicationContext());
        timetableDbAdapter.open();

        mNotification = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_stat_notification)
                .setContentTitle(getString(R.string.planHasChangedNotification))
                .setContentText(getString(R.string.planHasChangedNotification_summary))
                .setDefaults(Notification.DEFAULT_ALL)
                .setContentIntent(PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 0))
                .setAutoCancel(true);

        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                getCsvFromED();
            }
        };

        registerReceiver(mMessageReceiver, new IntentFilter("doUpdate"));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG_SERVICE, "service started");
        return START_STICKY;
    }

    private void getCsvFromED(){
        Log.i(TAG_SERVICE, "getting csv from ed");
        EDziekanatConnector getCSV = new EDziekanatConnector(getApplicationContext()){
            @Override
            public void onResponseReceived(ArrayList<String> result) {
                fillDatabaseFromED(result);
            }
        };

        getCSV.execute();
    }

    private void fillDatabaseFromED(ArrayList<String> downloadedCSV) {

        if(downloadedCSV == null){
            sendBroadcast(new Intent("downloadError"));
            return;
        }

        Cursor updateNeededCursor = timetableDbAdapter.getAllTimetable();
        if(updateNeededCursor.isAfterLast()){
            updateNeeded = true;
            Log.d(TimetableDbAdapter.DEBUG_TAG, "Baza danych pusta.");
        }

        ArrayList<String[]> csvSplitted = new ArrayList<>();

        Iterator it = downloadedCSV.iterator();
        it.next();
        updateNeededCursor.moveToFirst();
        for (String line; it.hasNext(); ){
            line = (String) it.next();
            String[] row = line.split(",");
            csvSplitted.add(row);
            if(!updateNeeded){
                if(
                        updateNeededCursor.getString(TimetableDbAdapter.TEMAT_COLUMN).equals(row[0]) &&
                                updateNeededCursor.getString(TimetableDbAdapter.LOC_COLUMN).equals(row[1]) &&
                                updateNeededCursor.getString(TimetableDbAdapter.DATAR_COLUMN).equals(row[2]) &&
                                updateNeededCursor.getString(TimetableDbAdapter.CZASR_COLUMN).equals(row[3]) &&
                                updateNeededCursor.getString(TimetableDbAdapter.DATAZ_COLUMN).equals(row[4]) &&
                                updateNeededCursor.getString(TimetableDbAdapter.CZASZ_COLUMN).equals(row[5])
                        ) {Log.d(TimetableDbAdapter.DEBUG_TAG, "Wszystko się zgadza!");}
                else {
                    updateNeeded = true;
                    Log.d(TimetableDbAdapter.DEBUG_TAG, "Wystąpiła rozbieżność");
                }
                updateNeededCursor.moveToNext();
                if(it.hasNext() && updateNeededCursor.isAfterLast()){
                    updateNeeded = true;
                }
                if(!updateNeededCursor.isAfterLast() && !it.hasNext()){
                    sendBroadcast(new Intent("downloadError"));
                }
            }
        }

        updateNeededCursor.close();

        if(updateNeeded) {
            Log.d(TimetableDbAdapter.DEBUG_TAG, "Rozpoczynam update bazy.");
            timetableDbAdapter.reinitiate();
            updateNeeded = false;

            timetableDbAdapter.insertMultiTimetable(csvSplitted);

            mNotificationManager.notify(0, mNotification.build());
        }

        sendBroadcast(new Intent("fillList"));
    }
}
