package pl.lukaszignaciuk.planwat2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Created by Łukasz on 2015-03-25.
 */
public class TimetableDbAdapter {
    public static final String DEBUG_TAG = "DEBUG_TAG_ADAPTER";

    public static final int DB_VERSION = 1;
    public static final String DB_NAME = "timetable.db";
    public static final String DB_SCHEDULE_TABLE = "timetable";

    public static final String KEY_ID = "_id";
    public static final String ID_OPTIONS = "INTEGER PRIMARY KEY AUTOINCREMENT";
    public static final int ID_COLUMN = 0;
    public static final String KEY_TEMAT = "Temat";
    public static final String TEMAT_OPTIONS = "TEXT NOT NULL";
    public static final int TEMAT_COLUMN = 1;
    public static final String KEY_LOC = "Lokalizacja";
    public static final String LOC_OPTIONS = "TEXT NOT NULL";
    public static final int LOC_COLUMN = 2;
    public static final String KEY_DATAR = "DataR";
    public static final String DATAR_OPTIONS = "TEXT NOT NULL";
    public static final int DATAR_COLUMN = 3;
    public static final String KEY_CZASR = "CzasR";
    public static final String CZASR_OPTIONS = "TEXT NOT NULL";
    public static final int CZASR_COLUMN = 4;
    public static final String KEY_DATAZ = "DataZ";
    public static final String DATAZ_OPTIONS = "TEXT NOT NULL";
    public static final int DATAZ_COLUMN = 5;
    public static final String KEY_CZASZ = "CzasZ";
    public static final String CZASZ_OPTIONS = "TEXT NOT NULL";
    public static final int CZASZ_COLUMN = 6;

    private static final String DB_CREATE_SCHEDULE_TABLE =
            "CREATE TABLE " + DB_SCHEDULE_TABLE+"( " +
                    KEY_ID + " " + ID_OPTIONS + ", " +
                    KEY_TEMAT + " " + TEMAT_OPTIONS + ", " +
                    KEY_LOC + " " + LOC_OPTIONS + ", " +
                    KEY_DATAR + " " + DATAR_OPTIONS + ", " +
                    KEY_CZASR + " " + CZASR_OPTIONS + ", " +
                    KEY_DATAZ + " " + DATAZ_OPTIONS + ", " +
                    KEY_CZASZ + " " + CZASZ_OPTIONS + " );";

    private static final String DB_DROP_SCHEDULE_TABLE =
            "DROP TABLE IF EXISTS " +  DB_SCHEDULE_TABLE;

    private SQLiteDatabase db;
    private Context context;
    private DatabaseHelper dbHelper;

    private static class DatabaseHelper extends SQLiteOpenHelper {

        private DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DB_CREATE_SCHEDULE_TABLE);

            Log.i(DEBUG_TAG, "Tworzenie bazy...");
            Log.i(DEBUG_TAG, "Tabela " + DB_SCHEDULE_TABLE + " ver." + DB_VERSION + " stworzona");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(DB_DROP_SCHEDULE_TABLE);

            Log.i(DEBUG_TAG, "Aktualizowanie bazy danych...");
            Log.i(DEBUG_TAG, "Wszystkie dany zostały usunięte.");

            onCreate(db);
        }
    }

    public TimetableDbAdapter(Context context) {
        this.context = context;
    }

    public TimetableDbAdapter open(){
        dbHelper = new DatabaseHelper(context, DB_NAME, null, DB_VERSION);
        try{
            db = dbHelper.getWritableDatabase();
        }
        catch (SQLException e){
            db = dbHelper.getReadableDatabase();
        }
        return this;
    }

    public void close(){
        dbHelper.close();
    }

    public void reinitiate(){
        db.execSQL(DB_DROP_SCHEDULE_TABLE);
        db.execSQL(DB_CREATE_SCHEDULE_TABLE);
    }

    public long insertTimetable(String temat, String lokalizacja,
                                String dataR, String czasR,
                                String dataZ, String czasZ){
        ContentValues newTimetableValues = new ContentValues();
        newTimetableValues.put(KEY_TEMAT, temat);
        newTimetableValues.put(KEY_LOC, lokalizacja);
        newTimetableValues.put(KEY_DATAR, dataR);
        newTimetableValues.put(KEY_CZASR, czasR);
        newTimetableValues.put(KEY_DATAZ, dataZ);
        newTimetableValues.put(KEY_CZASZ, czasZ);

        return db.insert(DB_SCHEDULE_TABLE, null, newTimetableValues);
    }

    public void insertMultiTimetable(ArrayList<String[]> csvSplitted){
        db.beginTransaction();

        for(String[] row : csvSplitted) {
            ContentValues newTimetableValues = new ContentValues();
            newTimetableValues.put(KEY_TEMAT, row[0]);
            newTimetableValues.put(KEY_LOC, row[1]);
            newTimetableValues.put(KEY_DATAR, row[2]);
            newTimetableValues.put(KEY_CZASR, row[3]);
            newTimetableValues.put(KEY_DATAZ, row[4]);
            newTimetableValues.put(KEY_CZASZ, row[5]);
            db.insert(DB_SCHEDULE_TABLE, null, newTimetableValues);
        }

        db.setTransactionSuccessful();
        db.endTransaction();
    }

    public Cursor getAllTimetable(){
        String[] columns = {KEY_ID, KEY_TEMAT, KEY_LOC, KEY_DATAR, KEY_CZASR, KEY_DATAZ, KEY_CZASZ};

        return db.query(DB_SCHEDULE_TABLE, columns, null, null, null, null, null);
    }

    public Cursor getSpecifiedDayTimetable(String date, boolean displayLectures){
        String[] columns = {KEY_ID, KEY_TEMAT, KEY_LOC, KEY_DATAR, KEY_CZASR, KEY_DATAZ, KEY_CZASZ};
        String where;
        String[] whereArgs;

        if(displayLectures){
            where = KEY_DATAR + "= ?";
            whereArgs = new String[]{date};
        }
        else {
            where = KEY_DATAR + "= ? AND " + KEY_TEMAT + " NOT LIKE ?";
            whereArgs = new String[]{date, "%(w)%"};
        }

        return db.query(DB_SCHEDULE_TABLE, columns, where, whereArgs, null, null, null);
    }

    public CharSequence[] getClassNames(){
        SortedSet<String> returnNames = new TreeSet<>();
        String[] columns = {KEY_ID, KEY_TEMAT};
        String name;

        Cursor classNames = db.query(DB_SCHEDULE_TABLE, columns, null, null, null, null, null);
        classNames.moveToFirst();

        while (!(classNames.isAfterLast())){
            name = classNames.getString(TEMAT_COLUMN);
            name = name.substring(0, name.lastIndexOf(")")+1);
            returnNames.add(name);
            classNames.moveToNext();
        }

        return returnNames.toArray(new CharSequence[0]);
    }

    public Cursor getClassTimetable(String className){
        String[] columns = {KEY_ID, KEY_TEMAT, KEY_LOC, KEY_DATAR, KEY_CZASR, KEY_DATAZ, KEY_CZASZ};
        String where;
        String[] whereArgs;

        where = KEY_TEMAT + " LIKE ?";
        whereArgs = new String[]{className + "%"};

        return db.query(DB_SCHEDULE_TABLE, columns, where, whereArgs, null, null, null);
    }

}